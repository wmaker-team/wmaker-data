wmaker-data (0.9~4-2) unstable; urgency=medium

  * d/control: set Multi-Arch to foreign
  * d/control: bump Standards-Version to 4.6.2
  * d/copyright: add myself to debian/*.

 -- Jeremy Sowden <jeremy@azazel.net>  Sat, 21 Jan 2023 14:32:24 +0000

wmaker-data (0.9~4-1) unstable; urgency=medium

  * Package salvaged by the Window Maker team (closes: #992953).
  * Package has had orig tar-balls since 0.9~1-2 (closes: #490538).
  * New upstream version 0.9~4
  * d/changelog: remove trailing white-space.
  * d/compat, d/control: remove compat and add a build-dep on
    `debhelper-compat = 13` to control.
  * d/control: change maintainer to WM team and add uploaders.
  * d/control: add VCS fields
  * d/control: set Rules-Requires-Root: no.
  * d/control: remove duplicate fields.
  * d/control: bump Standards-Version to 4.6.0
  * d/copyright: convert to DEP-5 format.
  * d/copyright: update location of Marcelo icons.
  * d/gbp.conf: add a gbp config file.
  * d/gbp.conf: add iconsPaul component to import-orig.
  * d/gbp.conf: exclude upstream debian directory.
  * d/gbp.conf, d/watch: add iconsMarcelo component.
  * d/gbp.conf: add dch commit-msg.
  * d/.gitignore: add a .gitignore file.
  * d/postinst: it's obsolete: remove it.
  * d/rules: simplify it.
  * d/watch: bump version.
  * d/watch: add component option, version and script for iconsPaul tar-ball.

 -- Jeremy Sowden <jeremy@azazel.net>  Thu, 16 Sep 2021 18:34:26 +0100

wmaker-data (0.9~3-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 16:46:58 +0100

wmaker-data (0.9~3-4) unstable; urgency=low

  * debian/rules: Changed 'dh_clean -i -k' by 'dh_prep -i'
  * debian/control: - Added 'Depends: ${misc:Depends}' in the binary
                      section
                    - Changed 'Build-Depends' on 'debhelper' to
                      correct version 7 as stated in debian/compat
                    - Updated 'Standards-Version' to '3.8.3'

 -- Noel David Torres Taño <envite@rolamasao.org>  Tue, 26 Jan 2010 22:30:35 +0100

wmaker-data (0.9~3-3) unstable; urgency=low

  * Corrected an error in the meaning of GPL-any in debian/copyright

 -- Noel David Torres Taño <envite@rolamasao.org>  Sun, 31 Aug 2008 14:08:24 +0200

wmaker-data (0.9~3-2) unstable; urgency=low

  * Separated by authors the debian/* files in debian/copyright

 -- Noel David Torres Taño <envite@rolamasao.org>  Wed, 27 Aug 2008 20:27:38 +0200

wmaker-data (0.9~3-1) unstable; urgency=low

  * Package ported to version 3.0 (quilt) of Debian source packages
    which is better for packages with several upstream sources like
    this one
  * Separated the icons from Paul Emsley in a different .orig tarball
    since I've found the upstream source
  * Added the Paul's tarball to the watch file from a new location,
    not the original one which is unversioned (and mostly dead)
  * Corrected and modernized the section of the debian/copyright
    about Paul files
  * Added the section of the debian/copyright about debian/* files
  * [OT] Updated the web page with this new tarball structure

 -- Noel David Torres Taño <envite@rolamasao.org>  Mon, 25 Aug 2008 06:14:46 +0200

wmaker-data (0.9~2-2) unstable; urgency=low

  * [OT] Created a web page for the .orig.tar.gz and other info
    (such as authoring and licensing) at
    http://rolamasao.org/wmaker-data
  * Added the field Homepage to debian/control
  * Added the file debian/watch to get ride of a Lintian warning

 -- Noel David Torres Taño <envite@rolamasao.org>  Fri, 22 Aug 2008 14:35:20 +0200

wmaker-data (0.9~2-1) unstable; urgency=low

  * Now using the repackaged .orig.tar.gz from Dmitry (unera)

 -- Noel David Torres Taño <envite@rolamasao.org>  Wed, 13 Aug 2008 05:45:51 +0200

wmaker-data (0.9~1-3) unstable; urgency=low

  * Changed the wmaker-data.install file from a list of all files to a
    glob pattern as suggested by Dmitry E. Oboukhov

 -- Noel David Torres Taño <envite@rolamasao.org>  Tue, 12 Aug 2008 18:57:50 +0200

wmaker-data (0.9~1-2) unstable; urgency=low

  * Added 'dh_install' to the 'rules' file.
  [unera]
  * Repackaged tar.gz for closes: 490538.

 -- Noel David Torres Taño <envite@rolamasao.org>  Mon, 11 Aug 2008 01:11:50 +0200

wmaker-data (0.9~1-1) unstable; urgency=low

  * Deleted the file /usr/share/icons/wmaker-GNUstep.xpm since it is
    bit-by-bit the same as /usr/share/WindowMaker/Icons/GNUstep.xpm
    from package wmaker

 -- Noel David Torres Taño <envite@rolamasao.org>  Thu, 07 Aug 2008 14:55:35 +0200

wmaker-data (0.8-3) unstable; urgency=low

  * New maintainer (Closes: #471992).
  * Synopsis adapted to actual policy.
  * Emacs setting dropped from changelog.
  * Added a reference to the GPL in the copyright file.
  * debhelper compatibility level set to V7 .
  * Added set -e to postinst to fill actual Policy requirements.

 -- Noel David Torres Taño <envite@rolamasao.org>  Mon, 07 Jul 2008 15:53:43 +0200

wmaker-data (0.8-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Get rid of the /usr/doc link in postinst (Closes: #359600).

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Tue, 18 Jul 2006 01:17:06 +0200

wmaker-data (0.8-2) unstable; urgency=low

  * Empty /usr/doc/wmaker-data (closes: bug#81025)
  * debian/control: Standards version 3.5.4.
  * debian/control: s/myoldaddress/mydebianaddress/

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sun,  3 Jun 2001 17:36:07 +0200

wmaker-data (0.8-1) unstable; urgency=low

  * Removed several groups of icons due to non-availability of copyright
    and/or appropiate licenses.  Upstreams are MIA and it's not possible to get
    confirmations on a case-by-case basis about the DFSG-status of the icons
    (closes: bug#68003)

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sun, 13 Sep 2000 19:01:00 +0200

wmaker-data (0.7-1) frozen unstable; urgency=low

  * Added icons for the Gimp
  * Moved icons to /usr/share/icons
  * debian/control: Fixed description (thanks to Alfredo for pointing this
    out)

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sun, 25 Oct 1998 20:28:19 -0600

wmaker-data (0.6-1) frozen unstable; urgency=low

  * Fixed a few icons that weren't tiffs but HTML docs instead.
    (closes: bug#21513)
  * Made this release 0.6-1 instead of 0.6 (after looking at some other
    packages that are compilations of someone else's work, like sgml-data)

 -- Marcelo E. Magallon <mmagallo@debian.org>  Sun, 26 Apr 1998 21:29:48 -0600

wmaker-data (0.5) unstable; urgency=low

  * Added more icons (Mutt, devices, clip, others)
  * Removed icons by Random (copyright problems?)
  * Reviewed the copyright statement

 -- Marcelo E. Magallon <mmagallo@debian.org>  Thu, 15 Mar 1998 8:45:00 -0600

wmaker-data (0.4) unstable; urgency=low

  * Add several more icons

 -- Marcelo E. Magallon <mmagallo@debian.org>  Thu, 15 Feb 1998 14:18:23 -0600

wmaker-data (0.3) unstable; urgency=low

  * Added an icon for PVM.

 -- Marcelo E. Magallon <mmagallo@debian.org>  Thu,  8 Jan 1998 23:12:11 -0600

wmaker-data (0.2) unstable; urgency=low

  * Added an (ugly but informative) default App Icon.

 -- Marcelo E. Magallon <mmagallo@debian.org>  Mon, 29 Dec 1997 10:40:02 -0600

wmaker-data (0.1) unstable; urgency=low

  * Initial Release.
  * Used icons from the Net that are GPLed.

 -- Marcelo E. Magallon <mmagallo@debian.org>  Thu, 18 Dec 1997 19:34:23 -0600
